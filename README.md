# CI Templates

Project requirements: 

* Create `.gitlab-ci.yml`
	- see [shinyLogger](https://gitlab.com/clmarquart/shinyLogger/)

* Create `pages` branch
	- needs /drat folder
	- needs /drat/index.html to avoid warnings
	- needs a `.gitlab-ci.yml` with the pages template
