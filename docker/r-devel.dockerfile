FROM rocker/r-ver:devel

ARG PKG_INSTALL

###
# OS packages
###
RUN apt-get update -y
RUN apt-get install -y apt-utils
RUN apt-get install -y libxml2 xml2
RUN apt-get install -y libxml2-dev
RUN apt-get install -y libcurl4-openssl-dev libssh2-1-dev libssl-dev git
RUN apt-get install -y qpdf pandoc pandoc-citeproc openssh-client curl
RUN apt-get install -y libgit2-dev

###
# R packages
###
RUN R -e 'install.packages(c("roxygen2","git2r","usethis","devtools","testthat"), repos="http://cran.rstudio.com", dependencies = TRUE)'

RUN R -e "install.packages(${PKG_INSTALL}, repos='http://cran.rstudio.com', dependencies=TRUE)"

RUN R --version